NAME := lin4qks

DIR_SRC := src
DIR_BUILD := build
DIR_BIN := bin

DIR_UTHASH := uthash

BIN := $(DIR_BIN)/$(NAME)

CPPFLAGS := -I$(DIR_SRC) \
            -I$(DIR_UTHASH)

CFLAGS := -std=gnu99 \
          -DYY_NO_INPUT \
          -ggdb \
           -Wall

LDFLAGS :=
LDLIBS := -lm -ldl -lgmp -lgsl -lgslcblas -lglpk -llinearizer -llpsolver

LEX := flex
LFLAGS :=

YACC := bison
YFLAGS := -d

LEXS := $(wildcard $(DIR_SRC)/*.l)
SRCS_LEX := $(LEXS:.l=.c)
YACCS := $(wildcard $(DIR_SRC)/*.y)
SRCS_YACC := $(YACCS:.y=.c)
HDRS_YACC := $(YACCS:.y=.h)
SRCS := $(wildcard $(DIR_SRC)/*.c) $(SRCS_LEX) $(SRCS_YACC)
DEPS := $(SRCS:$(DIR_SRC)/%.c=$(DIR_BUILD)/%.d)
OBJS := $(DEPS:.d=.o)

.PHONY: clean all install
.SECONDARY:

all: $(BIN)

$(BIN): $(OBJS)
	$(LINK.o) -o $@ $^ $(LDLIBS)

$(DIR_SRC)/%.c: $(DIR_SRC)/%.l
	$(LEX) $(LFLAGS) -o $@ $<

$(DIR_SRC)/%.c: $(DIR_SRC)/%.y
	$(YACC) $(YFLAGS) -o $@ $<

$(DIR_BUILD)/%.d: $(DIR_SRC)/%.c
	$(COMPILE.c) -MP -MM -MQ $(@:.d=.o) -MF $@ $<
$(DIR_BUILD)/%.o: $(DIR_SRC)/%.c Makefile
	$(COMPILE.c) -o $@ $<

install: all
	install $(BIN) /usr/local/bin

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPS)
endif

clean:
	$(RM) $(SRCS_LEX) $(SRCS_YACC) $(HDRS_YACC) $(DEPS) $(OBJS) $(BIN)
