double
ycos(double y, double x) {
  return y * cos(x);
}

double
ycos_lipschitz(double a1, double b1, double a2, double b2) {
  double m = fmax(fabs(b1), fabs(b2));
  return sqrt(1 + m * m);
}

double
sin_lipschitz(double a, double b) {
  return 1;
}
