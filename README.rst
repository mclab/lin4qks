=======
LIN4QKS
=======

LIN4QKS uses `LINEARIZER <https://bitbucket.org/mclab/linearizer>`_
library to overapproximate non-linear
Discrete Time Hybrid System :math:`\mathcal{H}` by means of
a piecewise affine system :math:`\mathcal{L}_{\mathcal{H}}` such that
controllers for :math:`\mathcal{L}_{\mathcal{H}}` are guaranteed to be
controllers for :math:`\mathcal{H}`.
Its output can be feeded to
`QKS <http://mclab.di.uniroma1.it/site/index.php/software/38-qks-intro>`_
to automatically synthesize control software.

Building prerequisites
======================

LIN4QKS library depends on the following external libraries:

* `GNU Scientific Library <http://www.gnu.org/software/gsl>`_
* `LINEARIZER <https://bitbucket.org/mclab/linearizer>`_

Usage instructions
==================
::

	Usage:
	        lin4qks -h
	        lin4qks [-i path] [-f path] [-o path] [-m num] [-v level]
	Where:
	        -h                      print help message and exit
	        -i path                 input file is at path (default is stdin)
	        -f path                 file with user defined functions is at path (default is "functions.c")
	        -o path                 output file is at path (default is stdout)
	        -m num                  number of sampling points (default is 80)
	        -v level                verbosity level is level (default is 0)

Example invocation:
::

	lin4qks -f examples/functions.c <examples/pendulum.m >pendulum.lin.m -m 40 -v 1

Input format
============

Tool takes as an input an extended version of QKS input format.
It is shipped with 2 example models:

* ``examples/pendulum.m``
* ``examples/pendulum_with_friction.m``

The difference with respect to QKS input format is that non-linear
sub-expressions are allowed in the transition relation.

Before using a function one must declare it: 
::

	Functions
	  function_name : function_arity : (period1, period2, ...)


Example:

::

	Functions
	  sin : 1 : (2 * pi);


If function is not periodic then periods list must be omitted.

After declaration function can be used in transition relation predicate
everywhere where a variable can be used, e.g.:
::

	x2' = x2 + T * ((g / l) * sin(x1, 0.3) + (1 / (m*l*l)) *  mult_u * u);


Additional numeric argument to the function represents upper bound for
the linearisation error.

And finally an additional C file must be given to lin4qks
containing:

1. definition of the function to be linearised,
   its name must match corresponding name in the input file
   (note however that all functions declared in ``math.h`` are automatically available)
2. definition of the function estimating lipschitz constant
   for the function to be linearised

Example for function :math:`y\cos{x}`:
::

	double ycos(x) {
	  return y * cos(x);
	}

	double
	ycos_lipschitz(double a1, double b1, double a2, double b2) {
	  double m = fmax(fabs(b1), fabs(b2));
	  return sqrt(1 + m * m);
	}

Function ``ycos_lipschitz`` estimates lipschitz constant of :math:`ycos{x}` on the interval :math:`[a_1,b_1]\times[a_2,b_2]`.
