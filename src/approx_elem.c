#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <lpsolver.h>
#include <linearizer.h>

#include <gmp.h>

#include <utarray.h>

#include "node.h"

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "func.h"
#include "lib.h"
#include "opts.h"
#include "keywords.h"
#include "node_helpers.h"
#include "func.h"
#include "gsl_helpers.h"
#include "algo_input.h"
#include "approx_elem.h"

struct Lin_approx_elem {
  gsl_vector *a;
  gsl_vector *b;
  gsl_vector *coeffs_over;
  gsl_vector *coeffs_under;
};

UT_icd lin_approx_elem_icd = {
  sizeof(Lin_approx_elem *),
  NULL,
  NULL,
  NULL
};

static node *
lin_coeffs_to_lin_expr(const gsl_vector * /*coeffs*/, Lin_algo_input * /*algo_input*/, unsigned long * /*ks*/);

Lin_approx_elem *
lin_approx_elem_init(gsl_vector *a, gsl_vector *b) {
  struct Lin_approx_elem *e;
  LIN_MALLOC(e, 1, struct Lin_approx_elem);
  e->a = a;
  e->b = b;
  e->coeffs_under = gsl_vector_alloc(a->size);
  e->coeffs_over = gsl_vector_alloc(a->size);
  return (Lin_approx_elem *) e;
}

void
lin_approx_elem_free(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  gsl_vector_free(e->a);
  gsl_vector_free(e->b);
  gsl_vector_free(e->coeffs_under);
  gsl_vector_free(e->coeffs_over);
  LIN_FREE(e);
}

Lin_approx_elem *
lin_approx_elem_dup(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  struct Lin_approx_elem *ed;
  LIN_MALLOC(ed, 1, struct Lin_approx_elem);
  ed->a = gsl_vector_alloc(e->a->size);
  gsl_vector_memcpy(ed->a, e->a);
  ed->b = gsl_vector_alloc(e->b->size);
  gsl_vector_memcpy(ed->a, e->b);
  ed->coeffs_under = gsl_vector_alloc(e->coeffs_under->size);
  gsl_vector_memcpy(ed->coeffs_under, e->coeffs_under);
  ed->coeffs_over = gsl_vector_alloc(e->coeffs_over->size);
  gsl_vector_memcpy(ed->coeffs_over, e->coeffs_over);
  return (Lin_approx_elem *) ed;
}

const gsl_vector *
lin_approx_elem_get_a(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return (const gsl_vector *) e->a;
}

const gsl_vector *
lin_approx_elem_get_b(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return (const gsl_vector *) e->b;
}

const gsl_vector *
lin_approx_elem_get_coeffs_under(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return e->coeffs_under;
}

void
lin_approx_elem_set_coeffs_under(Lin_approx_elem *_e, const gsl_vector *coeffs) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  gsl_vector_memcpy(e->coeffs_under, coeffs);
}

double
lin_approx_elem_get_coeff_under(Lin_approx_elem *_e, size_t i) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return gsl_vector_get(e->coeffs_under, i);
}

void
lin_approx_elem_set_coeff_under(Lin_approx_elem *_e, size_t i, double coeff) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  gsl_vector_set(e->coeffs_under, i, coeff);
}

const gsl_vector *
lin_approx_elem_get_coeffs_over(Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return e->coeffs_over;
}

void
lin_approx_elem_set_coeffs_over(Lin_approx_elem *_e, const gsl_vector *coeffs) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  gsl_vector_memcpy(e->coeffs_over, coeffs);
}

double
lin_approx_elem_get_coeff_over(Lin_approx_elem *_e, size_t i) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  return gsl_vector_get(e->coeffs_under, i);
}

void
lin_approx_elem_set_coeff_over(Lin_approx_elem *_e, size_t i, double coeff) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  gsl_vector_set(e->coeffs_over, i, coeff);
}

node *
lin_approx_elem_to_node(Lin_approx_elem *_e, Lin_algo_input *algo_input, size_t num, unsigned long *ks) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;
  Lin_func *f = lin_algo_input_get_func(algo_input);
  const char *name = lin_algo_input_get_name(algo_input);
  const gsl_vector *a = e->a;
  const gsl_vector *b = e->b;
  size_t i;
  size_t arity = lin_func_get_arity(f); 
  node *lin_expr;
  lin_expr = lin_coeffs_to_lin_expr(e->coeffs_under, algo_input, ks);
  node *lin_constr_under = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GE), new_id_node(strdup(name)), lin_expr);
  lin_expr = lin_coeffs_to_lin_expr(e->coeffs_over, algo_input, ks);
  node *lin_constr_over = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LE), new_id_node(strdup(name)), lin_expr);
  node *over_and_under = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), lin_constr_under, lin_constr_over);
  char *var_name;
  node *lb, *ub, *lb_constr, *ub_constr, *bound, *bounds;
  for (i = 0; i < arity; ++i) {
    if (ks[i] > 1) {
      lin_sprintf_smart(&var_name, "%s_%s", lin_algo_input_get_var_name(algo_input, i), lin_algo_input_get_name(algo_input));
    }
    else {
      var_name = strdup(lin_algo_input_get_var_name(algo_input, i));
    }
    lb = lin_new_realnumber_node(gsl_vector_get(a, i + 1));
    lb_constr = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_GE), new_id_node(var_name), lb);
    ub = lin_new_realnumber_node(gsl_vector_get(b, i + 1));
    ub_constr = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_LE), new_id_node(strdup(var_name)), ub);
    bound = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), lb_constr, ub_constr);
    if (i == 0) {
      bounds = bound;
    }
    else {
      bounds = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), bounds, bound);
    }
  }
  return lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), bounds, over_and_under);
}
void
lin_approx_elem_print(FILE *stream, Lin_approx_elem *_e) {
  struct Lin_approx_elem *e = (struct Lin_approx_elem *) _e;

  gsl_vector_const_view av = gsl_vector_const_subvector(e->a, 1, e->a->size - 1);
  lin_gsl_vector_print(stream, "a", &av.vector);

  gsl_vector_const_view bv = gsl_vector_const_subvector(e->b, 1, e->b->size - 1);
  lin_gsl_vector_print(stream, "b", &bv.vector);

  lin_gsl_vector_print(stream, "coeffs_under", e->coeffs_under);
  lin_gsl_vector_print(stream, "coeffs_over", e->coeffs_over);
}

static node *
lin_coeffs_to_lin_expr(const gsl_vector *coeffs, Lin_algo_input *algo_input, unsigned long *ks) {
  size_t i;
  size_t arity = coeffs->size - 1;
  char *var_name;
  node *lin_expr_elem;
  node *lin_expr = lin_new_realnumber_node(gsl_vector_get(coeffs, 0));
  for (i = 0; i < arity; ++i) {
    if (ks[i] > 1) {
      lin_sprintf_smart(&var_name, "%s_%s", lin_algo_input_get_var_name(algo_input, i), lin_algo_input_get_name(algo_input));
    }
    else {
      var_name = strdup(lin_algo_input_get_var_name(algo_input, i));
    }
    lin_expr_elem = lin_new_list_node_n(3,
    lin_new_keyword_node(LIN_KEYWORD_TIMES),
    lin_new_realnumber_node(gsl_vector_get(coeffs, i + 1)),
    new_id_node(var_name));
    lin_expr = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_PLUS), lin_expr_elem, lin_expr);
  }
  return lin_expr;
}
