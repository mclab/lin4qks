#ifndef LIN_ALGO_INPUT_H_
#define LIN_ALGO_INPUT_H_

typedef void Lin_algo_input;

extern Lin_algo_input *
lin_algo_input_init(Lin_func * /*f*/);

extern void
lin_algo_input_free(Lin_algo_input * /*_algo_input*/);

extern Lin_func *
lin_algo_input_get_func(Lin_algo_input * /*_algo_input*/);

extern void
lin_algo_input_set_func(Lin_algo_input * /*_algo_input*/, Lin_func * /*f*/);

extern const char *
lin_algo_input_get_name(Lin_algo_input * /*_algo_input*/);

extern void
lin_algo_input_set_name(Lin_algo_input * /*_algo_input*/, const char * /*name*/);

extern const char *
lin_algo_input_get_var_name(Lin_algo_input * /*_algo_input*/, size_t /*i*/);

extern void
lin_algo_input_set_var_name(Lin_algo_input * /*_algo_input*/, size_t /*i*/, const char * /*name*/);

extern const gsl_vector *
lin_algo_input_get_lbs(Lin_algo_input * /*_algo_input*/);

extern const gsl_vector *
lin_algo_input_get_ubs(Lin_algo_input * /*_algo_input*/);

extern double
lin_algo_input_get_lb(Lin_algo_input * /*_algo_input*/, size_t /*i*/);

extern void
lin_algo_input_set_lb(Lin_algo_input * /*_algo_input*/, size_t /*i*/, double /*v*/);

extern double
lin_algo_input_get_ub(Lin_algo_input * /*_algo_input*/, size_t /*i*/);

extern void
lin_algo_input_set_ub(Lin_algo_input * /*_algo_input*/, size_t /*i*/, double /*v*/);

extern unsigned int
lin_algo_input_get_m(Lin_algo_input * /*_algo_input*/);

extern void
lin_algo_input_set_m(Lin_algo_input * /*_algo_input*/, unsigned int /*m*/);

extern double
lin_algo_input_get_eps(Lin_algo_input * /*_algo_input*/);

extern void
lin_algo_input_set_eps(Lin_algo_input * /*_algo_input*/, double /*eps*/);

#endif /* #ifndef LIN_ALGO_INPUT_H_ */
