#define _SVID_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <dlfcn.h>

#include <sys/types.h>
#include <sys/wait.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "hashtable.h"
#include "func.h"
#include "lib.h"
#include "opts.h"
#include "log.h"

#define LIN_LIB_NAME "lib.so"

struct Lin_lib {
  char *path;
  void *handle;
  FILE *stream;
  Lin_hashtable *table;
};

Lin_lib *
lin_lib_init() {
  char *cmd_str;
  struct Lin_lib *lib;
  LIN_MALLOC(lib, 1, struct Lin_lib);
  lin_sprintf_smart(&lib->path, P_tmpdir "/" LIN_LIB_NAME);
  lib->handle = NULL;
  char *cc = getenv("CC");
  lin_sprintf_smart(&cmd_str, "%s -shared -fPIC -o %s -xc -lgsl -", cc ? cc : "cc", lib->path);
  lib->stream = popen(cmd_str, "w");
  LIN_FREE(cmd_str);
  fprintf(lib->stream, "#include <math.h>\n");
  fprintf(lib->stream, "#include <gsl/gsl_vector.h>\n");
  if (lin_opts_get_functions_path() != NULL) {
    fprintf(lib->stream, "#include \"%s\"", lin_opts_get_functions_path());
  }
  lib->table = lin_hashtable_init(NULL, lin_func_free);
  return lib;
}

void
lin_lib_free(Lin_lib *_lib) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  if (lib == NULL) {
    return;
  }
  if (lib->handle != NULL) {
    dlclose(lib->handle);
  }
  if (lib->stream != NULL) {
    pclose(lib->stream);
  }
  remove(lib->path);
  LIN_FREE(lib->path);
  lin_hashtable_free(lib->table);
  LIN_FREE(lib);
}

extern void
lin_lib_add_func(Lin_lib *_lib, const char *name, size_t arity, double *periods) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  size_t i;
  Lin_func *f = lin_func_init(arity, periods);
  lin_hashtable_insert(lib->table, name, f);
  fprintf(lib->stream, "\n");
  fprintf(lib->stream, "double\n");
  fprintf(lib->stream, "lin_%s(double *x) {\n", name);
  fprintf(lib->stream, "  return %s(", name);
  for (i = 0; i < arity; ++i) {
    fprintf(lib->stream, "x[%zu]", i);
    if (i < arity - 1) {
      fprintf(lib->stream, ", ");
    }
  }
  fprintf(lib->stream, ");");
  fprintf(lib->stream, "}\n");
  fprintf(lib->stream, "\n");
  fprintf(lib->stream, "double\n");
  fprintf(lib->stream, "lin_%s_lipschitz(gsl_vector *a, gsl_vector *b) {\n", name);
  fprintf(lib->stream, "  return %s_lipschitz(", name);
  for (i = 0; i < arity; ++i) {
    fprintf(lib->stream, "gsl_vector_get(a, %zu), gsl_vector_get(b, %zu)", i, i);
    if (i < arity - 1) {
      fprintf(lib->stream, ", ");
    }
  }
  fprintf(lib->stream, ");");
  fprintf(lib->stream, "}\n");
  fprintf(lib->stream, "\n");
  lin_log(2, "Added function \"%s\" with arity %zu to the library.", name, arity);
}

void
lin_lib_compile_and_load(Lin_lib *_lib) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  char *key;
  Lin_func *value;
  FuncPtr fptr;
  LipschitzFuncPtr fptr_lipschitz;
  int status;
  char *f_sym_name;
  if (lib->stream != NULL) {
    lin_log(1, "Library compilation started.");
    status = pclose(lib->stream);
    lib->stream = NULL;
    lin_fatalerror(!WIFEXITED(status), LIN_ERR_MSG_LIB_COMPILE);
    lin_fatalerror(WEXITSTATUS(status) != EXIT_SUCCESS, LIN_ERR_MSG_LIB_COMPILE);
    lin_log(1, "Library compilation finished.");
    lib->handle = dlopen(lib->path, RTLD_LAZY);
    lin_fatalerror(lib->handle == NULL, LIN_ERR_MSG_LIB_LOAD);
    lin_log(1, "Library loaded.");
    Lin_hashtable_iter *iter = lin_hashtable_iter_init(lib->table);
    while (lin_hashtable_iter_next(iter, &key, &value) == true) {
      lin_sprintf_smart(&f_sym_name, "lin_%s", key);
      fptr = dlsym(lib->handle, f_sym_name);
      lin_fatalerror(fptr == NULL, dlerror());
      lin_func_set_ptr(value, fptr); 
      LIN_FREE(f_sym_name);
      lin_sprintf_smart(&f_sym_name, "lin_%s_lipschitz", key);
      fptr_lipschitz = dlsym(lib->handle, f_sym_name);
      lin_func_set_ptr_lipschitz(value, fptr_lipschitz); 
      LIN_FREE(f_sym_name);
    }
    lin_hashtable_iter_free(iter);
  }
}

Lin_func *
lin_lib_get_func(Lin_lib *_lib, const char *name) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  Lin_func *res;
  lin_hashtable_lookup(lib->table, name, &res);
  return res;
}

bool
lin_lib_has_func(Lin_lib *_lib, const char *name) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  return lin_hashtable_lookup(lib->table, name, NULL);
}

void
lin_lib_reset_counts(Lin_lib *_lib) {
  struct Lin_lib *lib = (struct Lin_lib *) _lib;
  char *key;
  Lin_func *value;
  Lin_hashtable_iter *iter = lin_hashtable_iter_init(lib->table);
  while (lin_hashtable_iter_next(iter, &key, &value) == true) {
    lin_func_set_count(value, 0);
  }
  lin_hashtable_iter_free(iter);
}
