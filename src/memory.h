#ifndef LIN_MEMORY_H_
#define LIN_MEMORY_H_

#define LIN_MALLOC(var, count, type)                       \
do {                                                       \
  (var) = (type *) malloc((count) * sizeof(type));         \
  lin_fatalerror((var) == NULL, LIN_ERR_MSG_MEMORY_ALLOC); \
} while (0)

#define LIN_CALLOC(var, count, type)                       \
do {                                                       \
  (var) = (type *) calloc(count, sizeof(type));            \
  lin_fatalerror((var) == NULL, LIN_ERR_MSG_MEMORY_ALLOC); \
} while (0)

#define LIN_FREE(var) \
do {                  \
  free((var));        \
  (var) = NULL;       \
} while (0)

#endif /* #ifndef LIN_MEMORY_H_ */
