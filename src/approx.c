#include <stdlib.h>
#include <stdbool.h>
#include <float.h>
#include <math.h>
#include <string.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <lpsolver.h>
#include <linearizer.h>

#include <gmp.h>

#include <uthash.h>
#include <utarray.h>

#include "node.h"

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "keywords.h"
#include "node_helpers.h"
#include "func.h"
#include "gsl_helpers.h"
#include "algo_input.h"
#include "approx_elem.h"
#include "approx.h"

#define EQ_DOUBLE(x,y) (fabs(x-y) < 1e-3)

struct Lin_approx {
  Lin_func *f;
  UT_array *arr;
  double min;
  double max;
  unsigned long *ks;
};

static node *
lin_var_period_to_node(const char * /*name*/, const char * /*var_name*/, double /*period*/);

Lin_approx *
lin_approx_init(Lin_func *f) {
  struct Lin_approx *res;
  LIN_MALLOC(res, 1, struct Lin_approx);
  res->f = f;
  LIN_MALLOC(res->ks, lin_func_get_arity(f), size_t);
  utarray_new(res->arr, &lin_approx_elem_icd);
  res->min = DBL_MAX;
  res->max = -DBL_MAX;
  return (Lin_approx *) res;
}

void
lin_approx_free(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_approx_elem **e = NULL;
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    lin_approx_elem_free(*e);
  }
  utarray_free(approx->arr);
  LIN_FREE(approx->ks);
  LIN_FREE(approx);
}

void
lin_approx_clear(Lin_approx * _approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_approx_elem **e = NULL;
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    lin_approx_elem_free(*e);
  }
  utarray_clear(approx->arr);
}

void
lin_approx_print(FILE *stream, Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_approx_elem **e = NULL;
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    fprintf(stream, "\n");
    lin_approx_elem_print(stream, *e);
  }
}

Lin_func *
lin_approx_get_f(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return approx->f;
}

size_t
lin_approx_get_count(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return (size_t) utarray_len(approx->arr);
}

Lin_approx_elem *
lin_approx_get_elem(Lin_approx *_approx, size_t i) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_approx_elem **e = (Lin_approx_elem **) utarray_eltptr(approx->arr, i);
  return *e;
}

void
lin_approx_add_elem(Lin_approx *_approx, Lin_approx_elem *e) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  utarray_push_back(approx->arr, &e);

  double ya;
  const gsl_vector *a = lin_approx_elem_get_a(e);

  double yb;
  const gsl_vector *b = lin_approx_elem_get_b(e);

  const gsl_vector *coeffs_over = lin_approx_elem_get_coeffs_over(e);
  gsl_blas_ddot(a, coeffs_over, &ya);
  gsl_blas_ddot(b, coeffs_over, &yb);
  double max = (ya > yb) ? ya : yb;
  if (approx->max < max) {
    approx->max = max;
  }

  const gsl_vector *coeffs_under = lin_approx_elem_get_coeffs_under(e);
  gsl_blas_ddot(a, coeffs_under, &ya);
  gsl_blas_ddot(b, coeffs_under, &yb);
  double min = (ya < yb) ? ya : yb;
  if (approx->min > min) {
    approx->min = min;
  }
}

double
lin_approx_get_min(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return approx->min;
}

double
lin_approx_get_max(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return approx->max;
}

unsigned long *
lin_approx_get_ks(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return approx->ks;
}

void
lin_approx_set_ks(Lin_approx *_approx, unsigned long *ks) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_func *f = approx->f;
  size_t arity = lin_func_get_arity(f);
  memcpy(approx->ks, ks, arity * sizeof(unsigned long));
}

unsigned long
lin_approx_get_k(Lin_approx *_approx, size_t i) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  return approx->ks[i];
}

void
lin_approx_set_k(Lin_approx *_approx, size_t i, unsigned long k) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  approx->ks[i] = k;
}

void
lin_approx_compress(Lin_approx *_approx) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  UT_array *arr_c;
  utarray_new(arr_c, &lin_approx_elem_icd);
  Lin_approx_elem **e = (Lin_approx_elem **) utarray_front(approx->arr);
  Lin_approx_elem *e_last = lin_approx_elem_dup(*e);
  utarray_push_back(arr_c, &e_last);
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    if (lin_gsl_vectors_are_equal(lin_approx_elem_get_coeffs_under(*e), lin_approx_elem_get_coeffs_under(e_last)) &&
        lin_gsl_vectors_are_equal(lin_approx_elem_get_coeffs_over(*e), lin_approx_elem_get_coeffs_over(e_last))) {

    }
    else {
      e_last = lin_approx_elem_dup(*e);
    }
  }
}

node *
lin_approx_to_node(Lin_approx *_approx, Lin_algo_input *algo_input) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  size_t i;
  Lin_func *f = approx->f;
  size_t arity = lin_func_get_arity(f);
  node *approx_elem_node;
  node *approx_node;
  unsigned long *ks = lin_approx_get_ks(approx);
  Lin_approx_elem **e = NULL;
  i = 0;
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    approx_elem_node = lin_approx_elem_to_node(*e, algo_input, i, ks);
    if (i == 0) {
      approx_node = approx_elem_node;
    }
    else {
      approx_node = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_OR), approx_node, approx_elem_node);
    }
    ++i;
  }
  if (lin_func_is_periodic(f)) {
    node *var_period;
    for (i = 0; i < arity; ++i) {
      if (ks[i] > 1) {
        var_period = lin_var_period_to_node(lin_algo_input_get_name(algo_input),
        lin_algo_input_get_var_name(algo_input, i), lin_func_get_period(f, i));
        approx_node = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_AND), approx_node, var_period);
      }
    }
  }
  return approx_node;
}

void
lin_approx_print_gnuplot(Lin_approx *_approx, FILE *stream, const char *basename) {
  struct Lin_approx *approx = (struct Lin_approx *) _approx;
  Lin_approx_elem **e;
  Lin_approx_elem *e_front;
  Lin_approx_elem *e_back;
  const gsl_vector *coeffs;
  const gsl_vector *a;
  const gsl_vector *b;
  size_t count;

  e_front = *((Lin_approx_elem **) utarray_front(approx->arr));
  a = lin_approx_elem_get_a(e_front);
  e_back = *((Lin_approx_elem **) utarray_back(approx->arr));
  b = lin_approx_elem_get_b(e_back);

  fprintf(stream, "set terminal epslatex color colortext standalone\n");
  fprintf(stream, "set termoption dashed\n");
  fprintf(stream, "set output '%s.tex'\n", basename);
  fprintf(stream, "\n");

  fprintf(stream, "set style line 1 lt 1 lw 2 pt 3 lc rgb \"black\"\n");
  fprintf(stream, "set style line 2 lt 1 lw 2 pt 3 lc rgb \"red\"\n");
  fprintf(stream, "set style line 3 lt 1 lw 2 pt 3 lc rgb \"blue\"\n");
  fprintf(stream, "set style line 4 lt 3 lw 1 pt 3 lc rgb \"black\"\n");
  fprintf(stream, "set style line 5 lt 1 lw 1 pt 3 lc rgb \"black\"\n");
  fprintf(stream, "set style arrow 1 filled\n");
  fprintf(stream, "\n");

  /*fprintf(stream, "set xzeroaxis\n");*/
  /*fprintf(stream, "set xtics axis\n");*/
  /*fprintf(stream, "set format x '\\tiny %%g'\n");*/
  fprintf(stream, "unset xtics\n");
  fprintf(stream, "set xrange [%g:%g]\n", gsl_vector_get(a, 1), gsl_vector_get(b, 1));
  fprintf(stream, "set arrow from %g,0 to %g,0 ls 5 arrowstyle 1\n", 1.1 * gsl_vector_get(a, 1), 1.1 * gsl_vector_get(b, 1));
  fprintf(stream, "\n");

  /*fprintf(stream, "set yzeroaxis\n");*/
  /*fprintf(stream, "set ytics axis\n");*/
  /*fprintf(stream, "set format y '\\tiny %%g'\n");*/
  fprintf(stream, "unset ytics\n");
  fprintf(stream, "set yrange [%g:%g]\n", 1.5 * approx->min, 1.5 * approx->max);
  fprintf(stream, "set arrow from 0,%g to 0,%g ls 5 arrowstyle 1\n", 1.5 * approx->min, 1.5 * approx->max);
  fprintf(stream, "\n");

  fprintf(stream, "set samples 1001\n");
  fprintf(stream, "unset key\n");
  fprintf(stream, "set border 0\n");
  fprintf(stream, "\n");

  count = 0;
  e = NULL;
  double label_x;
  double label_x_u;
  double label_x_o;
  double label_y;
  double label_y_u;
  double label_y_o;
  fprintf(stream, "set arrow from %g,%g to %g,%g nohead ls 4\n", gsl_vector_get(a, 1), 1.5 * approx->min, gsl_vector_get(a, 1), 1.5 * approx->max);
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    a = lin_approx_elem_get_a(*e);
    b = lin_approx_elem_get_b(*e);

    label_x = (gsl_vector_get(b, 1) + gsl_vector_get(a, 1)) / 2;

    fprintf(stream, "set label '\\scriptsize $I_{%zu}$' at %g,%g center tc ls 1\n",
    count + 1, label_x, 1.5 * approx->min + 0.15);

    fprintf(stream, "set arrow from %g,%g to %g,%g nohead ls 4\n", gsl_vector_get(b, 1), 1.5 * approx->min, gsl_vector_get(b, 1), 1.5 * approx->max);
    coeffs = lin_approx_elem_get_coeffs_under(*e);
    fprintf(stream, "fu%zu(x) = (x >= %g) ? (x <= %g) ? %g + %g * x : 1/0 : 1/0\n",
    count, gsl_vector_get(a, 1), gsl_vector_get(b, 1), gsl_vector_get(coeffs, 0), gsl_vector_get(coeffs, 1));
    label_y = gsl_vector_get(coeffs, 0) + label_x * gsl_vector_get(coeffs, 1); 
    label_x_u = label_x;
    if (EQ_DOUBLE(gsl_vector_get(coeffs, 1), 0)) {
      label_y_u = label_y - 0.1;
    }
    else if (gsl_vector_get(coeffs, 1) > 0) {
      label_x_u += 0.1;
      label_y_u = label_y - 0.6;
    }
    else if (gsl_vector_get(coeffs, 1) < 0) {
      label_x_u -= 0.1;
      label_y_u = label_y - 0.6;
    }
    fprintf(stream, "set label '\\tiny $f_{%zu}^{-}(x)$' at %g,%g center tc ls 2\n",
    count + 1, label_x_u, label_y_u);

    coeffs = lin_approx_elem_get_coeffs_over(*e);
    fprintf(stream, "fo%zu(x) = (x >= %g) ? (x <= %g) ? %g + %g * x : 1/0 : 1/0\n",
    count, gsl_vector_get(a, 1), gsl_vector_get(b, 1), gsl_vector_get(coeffs, 0), gsl_vector_get(coeffs, 1));
    label_y = gsl_vector_get(coeffs, 0) + label_x * gsl_vector_get(coeffs, 1); 
    label_x_o = label_x;
    if (EQ_DOUBLE(gsl_vector_get(coeffs, 1), 0)) {
      label_y_o = label_y + 0.1;
    }
    else if (gsl_vector_get(coeffs, 1) > 0) {
      label_x_o -= 0.1;
      label_y_o = label_y + 0.6;
    }
    else if (gsl_vector_get(coeffs, 1) < 0) {
      label_x_o += 0.1;
      label_y_o = label_y + 0.6;
    }
    fprintf(stream, "set label '\\tiny $f_{%zu}^{+}(x)$' at %g,%g center tc ls 3\n",
    count + 1, label_x_o, label_y_o);
    ++count;
  }
  fprintf(stream, "\n");

  fprintf(stream, "plot sin(x) ls 1");
  count = 0;
  e = NULL;
  while ((e = (Lin_approx_elem **) utarray_next(approx->arr, e))) {
    fprintf(stream, ", fu%zu(x) ls 2, fo%zu(x) ls 3", count, count);
    ++count;
  }
  fprintf(stream, "\n");
}

static node *
lin_var_period_to_node(const char *name, const char *var_name, double period) {
  char *var_name_p;
  lin_sprintf_smart(&var_name_p, "%s_%s", var_name, name);
  char *var_name_k;
  lin_sprintf_smart(&var_name_k, "%s_k", var_name_p);
  node *res = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_TIMES), lin_new_realnumber_node(period), new_id_node(var_name_k));
  res = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_PLUS), res, new_id_node(var_name_p));
  res = lin_new_list_node_n(3, lin_new_keyword_node(LIN_KEYWORD_EQ), new_id_node(strdup(var_name)), res);
  return res;
}
