#ifndef LIN_GSL_HELPERS_H_
#define LIN_GSL_HELPERS_H_

extern gsl_vector *
lin_gsl_vector_alloc_from_array(size_t /*n*/, double * /*a*/);

extern gsl_matrix *
lin_gsl_matrix_alloc_from_array(size_t /*n1*/, size_t /*n2*/, double ** /*a*/);

extern double *
lin_gsl_array_alloc_from_vector(const gsl_vector * /*v*/);

extern void
lin_gsl_vector_print(FILE *f, const char * /*name*/, const gsl_vector * /*v*/);

extern gsl_vector *
lin_gsl_vector_copy(const gsl_vector * /*v*/);

extern bool
lin_gsl_vectors_are_equal(const gsl_vector * /*v1*/, const gsl_vector * /*v2*/);

#endif /* #ifndef LIN_GSL_HELPERS_H_ */
