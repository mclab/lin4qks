#ifndef LIN_OPTS_H_
#define LIN_OPTS_H_

#define LIN_OPTS_DEF_INPUT_FILE_PATH NULL
#define LIN_OPTS_DEF_OUTPUT_FILE_PATH NULL
#define LIN_OPTS_DEF_FUNCTIONS_PATH "functions.c"
#define LIN_OPTS_DEF_SAMPLING_POINTS_NUM 80
#define LIN_OPTS_DEF_VERBOSITY 0

extern void
lin_opts_init();

extern void
lin_opts_free();

extern void
lin_opts_print();

extern const char *
lin_opts_get_input_file_path();
extern void
lin_opts_set_input_file_path(const char * /*value*/);

extern const char *
lin_opts_get_output_file_path();
extern void
lin_opts_set_output_file_path(const char * /*value*/);

extern const char *
lin_opts_get_functions_path();
extern void
lin_opts_set_functions_path(const char * /*value*/);

extern unsigned int
lin_opts_get_sampling_points_num();
extern void
lin_opts_set_sampling_points_num(unsigned int /*value*/);

extern unsigned int
lin_opts_get_verbosity();
extern void
lin_opts_set_verbosity(unsigned int /*value*/);

extern Lin_lib *
lin_opts_get_lib();

extern void
lin_opts_set_lib(Lin_lib * /*lib*/);

#endif /* LIN_OPTS_H_ */
