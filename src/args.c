#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "types.h"
#include "errors.h"
#include "hashtable.h"
#include "func.h"
#include "lib.h"
#include "opts.h"
#include "args.h"

#define LIN_ARGS_HELP "h"
#define LIN_ARGS_INPUT_FILE_PATH "i"
#define LIN_ARGS_FUNCTIONS_PATH "f"
#define LIN_ARGS_OUTPUT_FILE_PATH "o"
#define LIN_ARGS_SAMPLING_POINTS_NUM "m"
#define LIN_ARGS_VERBOSITY "v"

static int
lin_args_parse_int_val(const char * /*val*/, void (* /*setter*/)(unsigned int));

void
lin_args_parse_cmd_line(int argc, char **argv) {
  const char *path = argv[0];
  const char *opt_name;

  --argc; ++argv;
  while (argc > 0) {
    opt_name = *argv;
    if (strcmp(opt_name, "-"LIN_ARGS_INPUT_FILE_PATH) == 0) {
      --argc; ++argv;
      lin_fatalerror(argc == 0, LIN_ERR_MSG_OPT_REQ_VAL, LIN_ARGS_INPUT_FILE_PATH);
      lin_opts_set_input_file_path(*argv);
      --argc; ++argv;
    }
    else if (strcmp(opt_name, "-"LIN_ARGS_OUTPUT_FILE_PATH) == 0) {
      --argc; ++argv;
      lin_fatalerror(argc == 0, LIN_ERR_MSG_OPT_REQ_VAL, LIN_ARGS_OUTPUT_FILE_PATH);
      lin_opts_set_output_file_path(*argv);
      --argc; ++argv;
    }
    else if (strcmp(opt_name, "-"LIN_ARGS_FUNCTIONS_PATH) == 0) {
      --argc; ++argv;
      lin_fatalerror(argc == 0, LIN_ERR_MSG_OPT_REQ_VAL, LIN_ARGS_FUNCTIONS_PATH);
      lin_opts_set_functions_path(*argv);
      --argc; ++argv;
    }
    else if (strcmp(opt_name, "-"LIN_ARGS_SAMPLING_POINTS_NUM) == 0) {
      --argc; ++argv;
      lin_fatalerror(argc == 0, LIN_ERR_MSG_OPT_REQ_VAL, LIN_ARGS_SAMPLING_POINTS_NUM);
      lin_fatalerror(lin_args_parse_int_val(*argv, &lin_opts_set_sampling_points_num), LIN_ERR_MSG_OPT_REQ_NUM_VAL, opt_name);
      --argc; ++argv;
    }
    else if (strcmp(opt_name, "-"LIN_ARGS_VERBOSITY) == 0) {
      --argc; ++argv;
      lin_fatalerror(argc == 0, LIN_ERR_MSG_OPT_REQ_VAL, LIN_ARGS_VERBOSITY);
      lin_fatalerror(lin_args_parse_int_val(*argv, &lin_opts_set_verbosity), LIN_ERR_MSG_OPT_REQ_POS_INT_VAL, opt_name);
      --argc; ++argv;
    }
    else if (strcmp(opt_name, "-"LIN_ARGS_HELP) == 0) {
      lin_args_print_usage(path);
      exit(EXIT_SUCCESS);
    }
    else {
      lin_fatalerror(1, LIN_ERR_MSG_OPT_UNKNOWN, opt_name);
    }
  }
}

void
lin_args_print_usage(const char *path) {
  fprintf(stderr, "\nUsage:\n");
  fprintf(stderr, "\t%s -%s\n", path, LIN_ARGS_HELP);
  fprintf(stderr, "\t%s [-%s path] [-%s path] [-%s path] [-%s num] [-%s level]\n", path,
  LIN_ARGS_INPUT_FILE_PATH, LIN_ARGS_FUNCTIONS_PATH, LIN_ARGS_OUTPUT_FILE_PATH,
  LIN_ARGS_SAMPLING_POINTS_NUM, LIN_ARGS_VERBOSITY);
  fprintf(stderr, "Where:\n");
  fprintf(stderr, "\t-%s\t\t\tprint help message and exit\n", LIN_ARGS_HELP);
  fprintf(stderr, "\t-%s path\t\t\tinput file is at path (default is %s)\n",
  LIN_ARGS_INPUT_FILE_PATH, "stdin");
  fprintf(stderr, "\t-%s path\t\t\tfile with user defined functions is at path (default is \"%s\")\n",
  LIN_ARGS_FUNCTIONS_PATH, lin_opts_get_functions_path());
  fprintf(stderr, "\t-%s path\t\t\toutput file is at path (default is %s)\n",
  LIN_ARGS_OUTPUT_FILE_PATH, "stdout");
  fprintf(stderr, "\t-%s num\t\t\tnumber of sampling points (default is %u)\n",
  LIN_ARGS_SAMPLING_POINTS_NUM, LIN_OPTS_DEF_SAMPLING_POINTS_NUM);
  fprintf(stderr, "\t-%s level\t\tverbosity level is level (default is %d)\n",
  LIN_ARGS_VERBOSITY, LIN_OPTS_DEF_VERBOSITY);
  fprintf(stderr, "\n");
}

static int
lin_args_parse_int_val(const char *val, void (*setter)(unsigned int)) {
  char *endptr;
  long num_val = strtol(val, &endptr, 10);
  if ((*endptr == '\0') && (num_val >= 0)) {
    setter((unsigned int) num_val);
    return 0;
  }
  else {
    return 1;
  }
}
