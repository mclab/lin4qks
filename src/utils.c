#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "errors.h"
#include "memory.h"
#include "utils.h"

extern FILE *
lin_file_open(const char *path) {
  FILE *res = fopen(path, "r");
  lin_fatalerror(res == NULL, LIN_ERR_MSG_FILE_CANNOT_OPEN, path);
  return res;
}

extern FILE *
lin_file_create(const char *path) {
  FILE *res = fopen(path, "w");
  lin_fatalerror(res == NULL, LIN_ERR_MSG_FILE_CANNOT_CREATE, path);
  return res;
}

extern int
lin_sprintf_smart(char **str, const char *fmt, ...) {
  *str = NULL;

  va_list args;

  va_start(args, fmt);
  int count = vsnprintf(NULL, 0, fmt, args);
  va_end(args);

  if (count >= 0) {
    char *buffer = NULL;
    LIN_MALLOC(buffer, count + 1, char);

    va_start(args, fmt);
    count = vsnprintf(buffer, count + 1, fmt, args);
    va_end(args);

    if (count < 0) {
      LIN_FREE(buffer);
      return count;
    }

    *str = buffer;
  }

  return count;
}

extern double
lin_ceil_decimal(double x) {
  int sgn = (x < 0) ? -1 : (x > 0);
  x = fabs(x) * 10;
  return sgn * ceil(x) / 10;
}

/*extern double*/
/*lin_time_ms() {*/
  /*struct timespec tv;*/
  /*clock_gettime(CLOCK_MONOTONIC, &tv);*/
  /*return ((double) tv.tv_sec) * 1E3 + ((double) tv.tv_nsec) / 1E6;*/
/*}*/

extern double
lin_time_ms_cpu() {
  struct rusage usage;
  int who = RUSAGE_SELF;
  getrusage(who, &usage);
  return ((double) usage.ru_utime.tv_sec) * 1E3 + ((double) usage.ru_utime.tv_usec) / 1E3; 
}
