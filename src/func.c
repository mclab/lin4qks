#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "errors.h"
#include "memory.h"
#include "types.h"
#include "func.h"

struct Lin_func {
  FuncPtr ptr;
  LipschitzFuncPtr ptr_lipschitz;
  size_t arity;
  double *periods;
  size_t count;
};

Lin_func *
lin_func_init(size_t arity, double *periods) {
  struct Lin_func *f;
  LIN_MALLOC(f, 1, struct Lin_func);
  f->count = 0;
  f->arity = arity;
  f->periods = periods;
  return (Lin_func *) f;
}

void
lin_func_free(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  LIN_FREE(f->periods);
  LIN_FREE(f);
}

size_t
lin_func_get_arity(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return f->arity;
}

FuncPtr
lin_func_get_ptr(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return f->ptr;
}

void
lin_func_set_ptr(Lin_func *_f, FuncPtr ptr) {
  struct Lin_func *f = (struct Lin_func *) _f;
  f->ptr = ptr;
}

LipschitzFuncPtr
lin_func_get_ptr_lipschitz(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return f->ptr_lipschitz;
}

void
lin_func_set_ptr_lipschitz(Lin_func *_f, LipschitzFuncPtr ptr_lipschitz) {
  struct Lin_func *f = (struct Lin_func *) _f;
  f->ptr_lipschitz = ptr_lipschitz;
}

size_t
lin_func_get_count(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return f->count;
}

void
lin_func_set_count(Lin_func *_f, size_t count) {
  struct Lin_func *f = (struct Lin_func *) _f;
  f->count = count;
}

double
lin_func_get_period(Lin_func *_f, size_t i) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return f->periods[i];
}

bool
lin_func_is_periodic(Lin_func *_f) {
  struct Lin_func *f = (struct Lin_func *) _f;
  return (bool) (f->periods != NULL);
}
