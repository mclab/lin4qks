#ifndef LIN_FUNC_H_
#define LIN_FUNC_H_

typedef void Lin_func;
typedef double (*Lin_fptr)(double *);

extern Lin_func *
lin_func_init(size_t /*arity*/, double * /*periods*/);

extern void
lin_func_free(Lin_func * /*_f*/);

size_t
lin_func_get_arity(Lin_func * /*_f*/);

extern FuncPtr
lin_func_get_ptr(Lin_func * /*_f*/);

extern void
lin_func_set_ptr(Lin_func * /*_f*/, FuncPtr /*ptr*/);

extern LipschitzFuncPtr
lin_func_get_ptr_lipschitz(Lin_func * /*_f*/);

extern void
lin_func_set_ptr_lipschitz(Lin_func * /*_f*/, LipschitzFuncPtr /*ptr*/);

extern size_t
lin_func_get_count(Lin_func * /*_f*/);

extern void
lin_func_set_count(Lin_func * /*_f*/, size_t /*count*/);

extern double
lin_func_get_period(Lin_func * /*_f*/, size_t /*i*/);

extern bool
lin_func_is_periodic(Lin_func * /*_f*/);

#endif /* #ifndef LIN_FUNC_H_ */
