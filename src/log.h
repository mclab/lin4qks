#ifndef LIN_LOG_H_
#define LIN_LOG_H_

void
lin_log(unsigned int /*level*/, const char * /*fmt*/, ...);

#endif /* #ifndef LIN_LOG_H_ */
