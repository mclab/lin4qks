#include <string.h>
#include <stdbool.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "types.h"
#include "func.h"
#include "algo_input.h"

struct Lin_algo_input {
  char *name;
  Lin_func *f;
  char **var_names;
  gsl_vector *lbs;
  gsl_vector *ubs;
  unsigned int m;
  double eps;
};

Lin_algo_input *
lin_algo_input_init(Lin_func *f) {
  size_t i;
  size_t arity;
  struct Lin_algo_input *algo_input;
  LIN_MALLOC(algo_input, 1, struct Lin_algo_input);
  algo_input->f = f;
  arity = lin_func_get_arity(f);
  algo_input->name = NULL;
  LIN_MALLOC(algo_input->var_names, arity, char *);
  for (i = 0; i < arity; ++i) {
    algo_input->var_names[i] = NULL;
  }
  algo_input->lbs = gsl_vector_alloc(arity);
  algo_input->ubs = gsl_vector_alloc(arity);
  return (Lin_algo_input *) algo_input;
}

void
lin_algo_input_free(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  size_t i;
  size_t arity = lin_func_get_arity(algo_input->f);
  for (i = 0; i < arity; ++i) {
    LIN_FREE(algo_input->var_names[i]);
  }
  LIN_FREE(algo_input->var_names);
  LIN_FREE(algo_input->name);
  gsl_vector_free(algo_input->lbs);
  gsl_vector_free(algo_input->ubs);
  LIN_FREE(algo_input);
}

const char *
lin_algo_input_get_name(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->name;
}

void
lin_algo_input_set_name(Lin_algo_input * _algo_input, const char *name) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  LIN_FREE(algo_input->name);
  algo_input->name = strdup(name);
}

const char *
lin_algo_input_get_var_name(Lin_algo_input * _algo_input, size_t i) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->var_names[i];
}

Lin_func *
lin_algo_input_get_func(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->f;
}

void
lin_algo_input_set_func(Lin_algo_input *_algo_input, Lin_func *f) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  algo_input->f = f;
}

void
lin_algo_input_set_var_name(Lin_algo_input * _algo_input, size_t i, const char *name) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  LIN_FREE(algo_input->var_names[i]);
  algo_input->var_names[i] = strdup(name);
}

const gsl_vector *
lin_algo_input_get_lbs(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->lbs;
}

const gsl_vector *
lin_algo_input_get_ubs(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->ubs;
}

double
lin_algo_input_get_lb(Lin_algo_input *_algo_input, size_t i) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return gsl_vector_get(algo_input->lbs, i);
}

void
lin_algo_input_set_lb(Lin_algo_input *_algo_input, size_t i, double v) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  gsl_vector_set(algo_input->lbs, i, v);
}

double
lin_algo_input_get_ub(Lin_algo_input *_algo_input, size_t i) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return gsl_vector_get(algo_input->ubs, i);
}

void
lin_algo_input_set_ub(Lin_algo_input *_algo_input, size_t i, double v) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  gsl_vector_set(algo_input->ubs, i, v);
}

unsigned int
lin_algo_input_get_m(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->m;
}

void
lin_algo_input_set_m(Lin_algo_input * _algo_input, unsigned int m) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  algo_input->m = m;
}

double
lin_algo_input_get_eps(Lin_algo_input *_algo_input) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  return algo_input->eps;
}

void
lin_algo_input_set_eps(Lin_algo_input * _algo_input, double eps) {
  struct Lin_algo_input *algo_input = (struct Lin_algo_input *) _algo_input;
  algo_input->eps = eps;
}
