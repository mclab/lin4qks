#ifndef __INTERNAL_H__
#define __INTERNAL_H__

#include <stdlib.h>
#include <stdio.h>

struct node {
  short type;
  char *value;
  int nchildren;
  struct node **children;
};

struct node *parsing_result;

struct node *newnode(int, char *, int, struct node **);

int print_tree_dot_rec(FILE *, struct node *);
int print_tree_dot_pretty_rec(FILE *, struct node *);

#endif

