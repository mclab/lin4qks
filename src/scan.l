%{
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <assert.h>

#include <gmp.h>

#include "node.h"

#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "parse.h"

extern void
yyerror(char * /*s*/);
%}

%x COMMENT_ONELINE
%x COMMENT_MULTILINE

%option yylineno
%option noyywrap
%option nounput

integerNumber (\+|-){0,1}[0-9]+
realNumber    (\+|-){0,1}[0-9]+\.[0-9]*((e|E)(\+|-){0,1}[0-9]+){0,1}|\.[0-9]+((e|E)(\+|-){0,1}[0-9]+){0,1}|[0-9]+(e|E)(\+|-){0,1}[0-9]+
id            [a-zA-Z0-9_@#$\^\?\.\']*

a [Aa]
b [Bb]
c [Cc]
d [Dd]
e [Ee]
f [Ff]
g [Gg]
h [Hh]
i [Ii]
j [Jj]
k [Kk]
l [Ll]
m [Mm]
n [Nn]
o [Oo]
p [Pp]
q [Qq]
r [Rr]
s [Ss]
t [Tt]
u [Uu]
v [Vv]
w [Ww]
x [Xx]
y [Yy]
z [Zz]

%%

[ \t\r\n]                                              ;

"("                                                    { return LPAREN; }
")"                                                    { return RPAREN; }
"{"                                                    { return LBRACE; }
"}"                                                    { return RBRACE; }
"["                                                    { return LBRACKET; }
"]"                                                    { return RBRACKET; }

{integerNumber}                                        {
                                                         yylval.string = strdup(yytext);
                                                         return NUMBER;
                                                       }

{realNumber}                                           {
                                                         yylval.string = strdup(yytext);
                                                         return REALNUMBER;
                                                       }

{c}{o}{n}{s}{t}                                        { return CONST; }
{t}{y}{p}{e}                                           { return TYPE; }
{s}{t}{a}{t}{e}{v}{a}{r}{s}                            { return STATEVARS; }
{i}{n}{p}{u}{t}{v}{a}{r}{s}                            { return INPUTVARS; }
{o}{u}{t}{p}{u}{t}{v}{a}{r}{s}                         { return OUTPUTVARS; }
{t}{r}{a}{n}{s}                                        { return TRANS; }
{s}{a}{f}{e}{t}{y}                                     { return SAFETY; }
{c}{o}{n}{t}{r}{o}{l}{l}{a}{b}{l}{e}{r}{e}{g}{i}{o}{n} { return CONTROLLABLEREGION; }
{g}{o}{a}{l}                                           { return GOAL; }
{o}{b}{s}{e}{r}{v}{a}{t}{i}{o}{n}                      { return OBSERVATION; }
{f}{u}{n}{c}{t}{i}{o}{n}{s}                            { return FUNCTIONS; }

{f}{o}{r}{a}{l}{l}                                     { return FORALL; }
{e}{x}{i}{s}{t}{s}                                     { return EXISTS; }

{e}{n}{u}{m}                                           { return ENUM; }

{t}{r}{u}{e}                                           { return TRUE; }
{f}{a}{l}{s}{e}                                        { return FALSE; }

"!"                                                    { return NOT; }
"&"                                                    { return AND; }
"|"                                                    { return OR; }
"->"                                                   { return IMPLY; }
".."                                                   { return DOTDOT; }
";"                                                    { return SEMICOLON; }
":"                                                    { return COLON; }
","                                                    { return COMMA; }
"+"                                                    { return PLUS; }
"-"                                                    { return MINUS; }
"*"                                                    { return TIMES; }
"/"[^"/*"]                                             { return DIVIDE; }
"%"                                                    { return MOD; }
">="                                                   { return GEQ; }
">"                                                    { return GT; }
"<="                                                   { return LEQ; }
"<"                                                    { return LT; }
"="                                                    { return EQ; }

{id}                                                   {
                                                         yylval.string = strdup(yytext);
                                                         return ID;
                                                       }

"//"                                                   { BEGIN(COMMENT_ONELINE); }
<COMMENT_ONELINE>[^\n]                                 ;
<COMMENT_ONELINE>\n                                    { BEGIN(INITIAL); }

"/*"                                                   { BEGIN(COMMENT_MULTILINE); }
<COMMENT_MULTILINE>[^*\n]*                             ;
<COMMENT_MULTILINE>"*"                                 ;
<COMMENT_MULTILINE>"*/"                                { BEGIN(INITIAL); }

.                                                      {
                                                         char *str;
                                                         lin_sprintf_smart(&str, "illegal token %s", yytext);
                                                         yyerror(str);
                                                         LIN_FREE(str);
                                                       }


%%
