#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include <stdlib.h>
#include <stdio.h>

typedef void node;

int elparsing(FILE *);

/* create a new node */
node *new_realnumber_node(char *);
node *new_number_node(char *);
node *new_id_node(char *);
node *new_list_node(char *, int, node **);

/* copy node */
node *copy_node(node *);

/* dispose tree */
void free_node(node *);
void free_parsing_result();

/* parsing result */
node *get_parsing_result();
void set_parsing_result(node *);

/* node type */
int is_realnumber(node *);
int is_number(node *);
int is_id(node *);
int is_list(node *);

/* node value */
char *get_value(node *);
void set_value(node *, char *);

/* node children */
int get_children_num(node *);
void set_children_num(node *, int);
node **get_children(node *);
void set_children(node *, node **);
node *get_ith_child(node *, int);
void set_ith_child(node *, int, node *);

/* debugging */
void print_tree(FILE *, node *);
void print_tree_pretty(FILE *, node *);
void print_tree_dot(FILE *, node *);
void print_tree_dot_pretty(FILE *, node *);

#endif

