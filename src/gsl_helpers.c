#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multimin.h>

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "gsl_helpers.h"

extern gsl_vector *
lin_gsl_vector_alloc_from_array(size_t n, double *a) {
  size_t i;
  gsl_vector *res = gsl_vector_alloc(n);
  for (i = 0; i < n; ++i) {
    gsl_vector_set(res, i, a[i]);
  }
  return res;
}

extern gsl_matrix *
lin_gsl_matrix_alloc_from_array(size_t n1, size_t n2, double **a) {
  size_t i, j;
  gsl_matrix *res = gsl_matrix_alloc(n1, n2);
  for (i = 0; i < n1; ++i) {
    for (j = 0; j < n2; ++j) {
      gsl_matrix_set(res, i, j, a[i][j]);
    }
  }
  return res;
}

extern double *
lin_gsl_array_alloc_from_vector(const gsl_vector *v) {
  size_t i;
  double *res;
  LIN_MALLOC(res, v->size, double);
  for (i = 0; i < v->size; ++i) {
    res[i] = gsl_vector_get(v, i);
  }
  return res;
}

extern void
lin_gsl_vector_print(FILE *f, const char *name, const gsl_vector *v) {
  size_t i;
  fprintf(f, "%s = (", name);
  for (i = 0; i < v->size; ++i) {
    fprintf(f, " %g", gsl_vector_get(v, i));
  }
  fprintf(f, " )\n");
}

extern gsl_vector *
lin_gsl_vector_copy(const gsl_vector *v) {
  gsl_vector *res = gsl_vector_alloc(v->size);
  gsl_vector_memcpy(res, v);
  return res;
}

extern bool
lin_gsl_vectors_are_equal(const gsl_vector *v1, const gsl_vector *v2) {
  size_t i;
  if (v1->size != v2->size) {
    return false;
  }
  for (i = 0; i < v1->size; ++i) {
    if (gsl_vector_get(v1, i) != gsl_vector_get(v2, i)) {
      break;
    }
  }
  if (i < v1->size) {
    return false;
  }
  else {
    return true;
  }
}
