#ifndef LIN_APPROX_ELEM_H_
#define LIN_APPROX_ELEM_H_

typedef void Lin_approx_elem;

extern Lin_approx_elem *
lin_approx_elem_init(gsl_vector * /*a*/, gsl_vector * /*b*/);

extern void
lin_approx_elem_free(Lin_approx_elem * /*_e*/);

extern Lin_approx_elem *
lin_approx_elem_dup(Lin_approx_elem * /*_e*/);

extern const gsl_vector *
lin_approx_elem_get_a(Lin_approx_elem * /*_e*/);

extern const gsl_vector *
lin_approx_elem_get_b(Lin_approx_elem * /*_e*/);

extern const gsl_vector *
lin_approx_elem_get_coeffs_under(Lin_approx_elem * /*_e*/);

extern void
lin_approx_elem_set_coeffs_under(Lin_approx_elem * /*_e*/, const gsl_vector * /*coeffs*/);

extern double
lin_approx_elem_get_coeff_under(Lin_approx_elem * /*_e*/, size_t /*i*/);

extern void
lin_approx_elem_set_coeff_under(Lin_approx_elem * /*_e*/, size_t /*i*/, double /*coeff*/);

extern const gsl_vector *
lin_approx_elem_get_coeffs_over(Lin_approx_elem * /*_e*/);

extern void
lin_approx_elem_set_coeffs_over(Lin_approx_elem * /*_e*/, const gsl_vector * /*coeffs*/);

extern double
lin_approx_elem_get_coeff_over(Lin_approx_elem * /*_e*/, size_t /*i*/);

extern void
lin_approx_elem_set_coeff_over(Lin_approx_elem * /*_e*/, size_t /*i*/, double /*coeff*/);

extern node *
lin_approx_elem_to_node(Lin_approx_elem * /*_e*/, Lin_algo_input * /*algo_input*/, size_t /*num*/, unsigned long * /*ks*/);

extern void
lin_approx_elem_print(FILE * /*stream*/, Lin_approx_elem * /*_e*/);

extern UT_icd lin_approx_elem_icd;

#endif /* #ifndef LIN_APPROX_ELEM_H_ */
