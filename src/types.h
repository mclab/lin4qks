#ifndef LIN_TYPES_H_
#define LIN_TYPES_H_

#ifndef bool
enum bool {
  false = 0,
  true
};
typedef enum bool bool;
#endif

#endif /* #ifndef LIN_TYPES_H_ */
