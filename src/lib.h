#ifndef LIN_LIB_H_
#define LIN_LIB_H_

typedef void Lin_lib;

extern Lin_lib *
lin_lib_init();

extern void
lin_lib_free(Lin_lib * /*_lib*/);

extern void
lin_lib_add_func(Lin_lib * /*_lib*/, const char * /*name*/, size_t /*arity*/, double * /*periods*/);

extern void
lin_lib_compile_and_load(Lin_lib * /*_lib*/);

extern Lin_func *
lin_lib_get_func(Lin_lib * /*_lib*/, const char * /*name*/);

extern bool
lin_lib_has_func(Lin_lib * /*_lib*/, const char * /*name*/);

extern void
lin_lib_reset_counts(Lin_lib * /*_lib*/);

#endif /* #ifndef LIB_LIB_H_ */
