#ifndef LIN_HASHTABLE_H_
#define LIN_HASHTABLE_H_

typedef void Lin_hashtable;
typedef void (*Lin_hashtable_value_copy_f)(void *, const void *);
typedef void (*Lin_hashtable_value_dtor_f)(void *);
typedef void Lin_hashtable_iter;

extern Lin_hashtable *
lin_hashtable_init(Lin_hashtable_value_copy_f /*value_copy_f*/, Lin_hashtable_value_dtor_f /*value_dtor_f*/);

extern void
lin_hashtable_free(Lin_hashtable * /*_table*/);

extern void
lin_hashtable_insert(Lin_hashtable * /*_table*/, const char * /*key*/, void * /*value*/);

extern bool
lin_hashtable_lookup(Lin_hashtable * /*_table*/, const char * /*key*/, void ** /*value*/);

extern bool
lin_hashtable_remove(Lin_hashtable * /*_table*/, const char * /*key*/);

extern void
lin_hashtable_clear(Lin_hashtable * /*_table*/);

extern size_t
lin_hashtable_size(Lin_hashtable * /*_table*/);

extern Lin_hashtable_iter *
lin_hashtable_iter_init(Lin_hashtable * /*_table*/); 

extern void
lin_hashtable_iter_free(Lin_hashtable_iter * /*_iter*/);

extern bool
lin_hashtable_iter_next(Lin_hashtable_iter * /*_iter*/, char ** /*key*/, void ** /*value*/);

#endif /* #ifndef LIN_HASHTABLE_H_ */
