#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include <lpsolver.h>
#include <linearizer.h>

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "hashtable.h"
#include "func.h"
#include "lib.h"
#include "opts.h"

typedef struct Lin_opts Lin_opts;

struct Lin_opts {
  char *input_file_path;
  char *output_file_path;
  char *functions_path;
  unsigned int sampling_points_num;
  unsigned int verbosity;
  Lin_lib *lib;
};

static Lin_opts *lin_opts = NULL;

void
lin_opts_init() {
  LIN_CALLOC(lin_opts, 1, Lin_opts);
  lin_opts_set_input_file_path(LIN_OPTS_DEF_INPUT_FILE_PATH);
  lin_opts_set_output_file_path(LIN_OPTS_DEF_OUTPUT_FILE_PATH);
  lin_opts_set_functions_path(LIN_OPTS_DEF_FUNCTIONS_PATH);
  lin_opts_set_sampling_points_num(LIN_OPTS_DEF_SAMPLING_POINTS_NUM);
  lin_opts_set_verbosity(LIN_OPTS_DEF_VERBOSITY);
}

void
lin_opts_free() {
  LIN_FREE(lin_opts->input_file_path);
  LIN_FREE(lin_opts->output_file_path);
  LIN_FREE(lin_opts->functions_path);
  lin_lib_free(lin_opts->lib);

  LIN_FREE(lin_opts);
}

void
lin_opts_print() {
  fprintf(stderr, "Current options:\n");
  fprintf(stderr, "\tinput file path:\t\t\t\t\t\t\"%s\"\n", lin_opts_get_input_file_path());
  fprintf(stderr, "\tpath to file with user defined functions:\t\t\t\"%s\"\n", lin_opts_get_functions_path());
  fprintf(stderr, "\toutput file path:\t\t\t\t\t\t\"%s\"\n", lin_opts_get_output_file_path());
  fprintf(stderr, "\tverbosity level:\t\t\t\t\t\t%u\n", lin_opts_get_verbosity());
}

const char *
lin_opts_get_input_file_path() {
  return lin_opts->input_file_path;
}

void
lin_opts_set_input_file_path(const char *value) {
  LIN_FREE(lin_opts->input_file_path);
  lin_opts->input_file_path = value ? strdup(value) : NULL;
}

const char *
lin_opts_get_output_file_path() {
  return lin_opts->output_file_path;
}

void
lin_opts_set_output_file_path(const char *value) {
  LIN_FREE(lin_opts->output_file_path);
  lin_opts->output_file_path = value ? strdup(value) : NULL;
}

const char *
lin_opts_get_functions_path() {
  return lin_opts->functions_path;
}

void
lin_opts_set_functions_path(const char *value) {
  LIN_FREE(lin_opts->functions_path);
  lin_opts->functions_path = value ? strdup(value) : NULL;
}

unsigned int
lin_opts_get_sampling_points_num() {
  return lin_opts->sampling_points_num;
}

void
lin_opts_set_sampling_points_num(unsigned int value) {
  lin_opts->sampling_points_num = value;
}

unsigned int
lin_opts_get_verbosity() {
  return lin_opts->verbosity;
}

void
lin_opts_set_verbosity(unsigned int value) {
  lin_opts->verbosity = value;
}

Lin_lib *
lin_opts_get_lib() {
  return lin_opts->lib;
}

void
lin_opts_set_lib(Lin_lib *lib) {
  lin_opts->lib = lib;
}
