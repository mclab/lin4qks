#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

#include <lpsolver.h>
#include <linearizer.h>

#include <glpk.h>

#include <utarray.h>

#include "node.h"

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "func.h"
#include "lib.h"
#include "opts.h"
#include "log.h"
#include "algo_input.h"
#include "approx_elem.h"
#include "approx.h"
#include "algo.h"

extern Lin_approx *
lin_algo_exec(Lin_algo_input *algo_input) {
  double time_start;
  double time_finish;
  Lin_func *f = lin_algo_input_get_func(algo_input);
  time_start = lin_time_ms_cpu();
  lin_log(1, "-----------------------------");
  lin_log(1, "Finding approximation for %s.", lin_algo_input_get_name(algo_input));
  size_t arity = lin_func_get_arity(f);
  double eps = lin_algo_input_get_eps(algo_input);
  unsigned int m = lin_algo_input_get_m(algo_input);
  gsl_vector *a = gsl_vector_alloc(arity);
  gsl_vector_memcpy(a, lin_algo_input_get_lbs(algo_input));
  gsl_vector *b = gsl_vector_alloc(arity);
  gsl_vector_memcpy(b, lin_algo_input_get_ubs(algo_input));
  double width, middle, period;
  unsigned long k;
  unsigned long *ks;
  LIN_CALLOC(ks, arity, unsigned long);
  size_t i;
  if (lin_func_is_periodic(f)) {
    for (i = 0; i < arity; ++i) {
      width = gsl_vector_get(b, i) - gsl_vector_get(a, i);
      middle = gsl_vector_get(a, i) + width / 2;
      period = lin_func_get_period(f, i);
      if (width <= period) {
        k = 1;
      }
      else {
        gsl_vector_set(a, i, middle - period / 2);
        gsl_vector_set(b, i, middle + period / 2);
        k = ((unsigned long) ceil(width / period));
      }
      ks[i] = k;
    }
  }
  LPSolver *solver = lpsolver_new(LPSOLVER_BACKEND_TYPE_GLPK);
  Linearization *linearization = linearization_new(
    lin_func_get_ptr(f),
    arity,
    lin_func_get_ptr_lipschitz(f),
    a,
    b,
    eps,
    m,
    solver,
    NULL
  );
  Lin_approx *res = lin_approx_init(f);
  Lin_approx_elem *elem;
  gsl_vector *elem_a;
  gsl_vector *elem_b;
  gsl_vector_view subv;
  for (i = 0; i < lin_cover_get_num(linearization->cover); ++i) {
    gsl_vector_const_view a_v = lin_cover_get_interval_lb(linearization->cover, i);
    elem_a = gsl_vector_alloc(arity + 1);
    gsl_vector_set(elem_a, 0, 1);
    subv = gsl_vector_subvector(elem_a, 1, arity);
    gsl_vector_memcpy(&subv.vector, &a_v.vector);
    gsl_vector_const_view b_v = lin_cover_get_interval_ub(linearization->cover, i);
    elem_b = gsl_vector_alloc(arity + 1);
    gsl_vector_set(elem_b, 0, 1);
    subv = gsl_vector_subvector(elem_b, 1, arity);
    gsl_vector_memcpy(&subv.vector, &b_v.vector);
    elem = lin_approx_elem_init(elem_a, elem_b);
    gsl_vector_const_view c_under = gsl_matrix_const_row(linearization->f_minus, i);
    lin_approx_elem_set_coeffs_under(elem, &c_under.vector);
    gsl_vector_const_view c_over = gsl_matrix_const_row(linearization->f_plus, i);
    lin_approx_elem_set_coeffs_over(elem, &c_over.vector);
    lin_approx_add_elem(res, elem);
  }
  lin_approx_set_ks(res, ks);
  if (lin_opts_get_verbosity() >= 2) {
    lin_approx_print(stderr, res);
    fprintf(stderr, "\n");
  }
  gsl_vector_free(a);
  gsl_vector_free(b);
  time_finish = lin_time_ms_cpu();
  lin_log(1, "Number of intervals: %zu", lin_approx_get_count(res));
  lin_log(1, "CPU time spent: %g", time_finish - time_start);
  lin_log(1, "-----------------------------");
  return res;
}
