#ifndef LIN_APPROX_H_
#define LIN_APPROX_H_

typedef void Lin_approx;

extern Lin_approx *
lin_approx_init(Lin_func * /*f*/);

extern void
lin_approx_free(Lin_approx * /*_approx*/);

extern void
lin_approx_clear(Lin_approx * /*_approx*/);

extern void
lin_approx_print(FILE * /*stream*/, Lin_approx * /*_approx*/);

extern Lin_func *
lin_approx_get_f(Lin_approx * /*_approx*/);

extern size_t
lin_approx_get_count(Lin_approx * /*_approx*/);

extern Lin_approx_elem *
lin_approx_get_elem(Lin_approx * /*_approx*/, size_t /*i*/);

extern void
lin_approx_add_elem(Lin_approx * /*_approx*/, Lin_approx_elem * /*e*/);

extern double
lin_approx_get_min(Lin_approx * /*_approx*/);

extern double
lin_approx_get_max(Lin_approx * /*_approx*/);

extern unsigned long *
lin_approx_get_ks(Lin_approx * /*_approx*/);

extern void
lin_approx_set_ks(Lin_approx * /*_approx*/, unsigned long * /*ks*/);

extern unsigned long
lin_approx_get_k(Lin_approx * /*_approx*/, size_t /*i*/);

extern void
lin_approx_set_k(Lin_approx * /*_approx*/, size_t /*i*/, unsigned long /*k*/);

extern void
lin_approx_compress(Lin_approx * /*_approx*/);

extern node *
lin_approx_to_node(Lin_approx *approx, Lin_algo_input * /*algo_input*/);

extern void
lin_approx_print_gnuplot(Lin_approx * /*_approx*/, FILE * /*stream*/, const char * /*basename*/);

#endif /* LIN_APPROX_H_ */
