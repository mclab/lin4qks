#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <gmp.h>

#include "node.h"

#include "types.h"
#include "errors.h"
#include "memory.h"
#include "utils.h"
#include "keywords.h"
#include "node_helpers.h"

static void
lin_expr_calc_rec(mpq_t /*res*/, node * /*expr*/);

node *
lin_new_list_node(size_t children_num, node **children) {
  return new_list_node((char *) 1, children_num, children);
}

node *
lin_new_list_node_n(size_t num, ...) {
  size_t i;
  node **children;
  LIN_MALLOC(children, num, node *);
  va_list args;
  va_start(args, num);
  for (i = 0; i < num; ++i) {
    children[i] = va_arg(args, node *);
  }
  va_end(args);
  return lin_new_list_node(num, children);
}

node *
lin_new_keyword_node(Lin_keyword_id id) {
  return new_id_node(strdup(lin_keywords_names[id]));
}

bool
lin_is_keyword_node(node *n, Lin_keyword_id id) {
  if (is_id(n) && (strcmp(lin_keywords_names[id], get_value(n)) == 0)) {
    return true;
  }
  else {
    return false;
  }
}

node *
lin_add_node_to_list(node *n, node *list) {
  if (list == NULL) {
    return lin_new_list_node_n(1, n);
  }
  else {
    size_t i;
    size_t children_num = get_children_num(list);
    node **children;
    LIN_MALLOC(children, children_num + 1, node *);
    children[0] = n;
    for (i = 0; i < children_num; ++i) {
      children[i + 1] = copy_node(get_ith_child(list, i));
    }
    free_node(list);
    return lin_new_list_node(children_num + 1, children);
  }
}

node *
lin_expr_calc(node *expr) {
  node *res;
  mpq_t q;
  mpq_init(q);
  lin_expr_calc_rec(q, expr);
  res = lin_mpq_get_node(q);
  mpq_clear(q);
  return res;
}

node *
lin_mpq_get_node(mpq_t q) {
  node *res;
  mpz_t num, den;
  mpz_inits(num, den, NULL);
  mpq_get_num(num, q);
  mpq_get_den(den, q);
  if (mpz_cmp_si(den, 1) == 0) {
    res = lin_mpz_get_node(num);
  }
  else {
    double d = mpq_get_d(q);
    char *d_str;
    lin_sprintf_smart(&d_str, "%g", d);
    res = new_realnumber_node(d_str);
  }
  mpz_clears(num, den, NULL);
  return res;
}

node *
lin_mpz_get_node(mpz_t z) {
  return new_number_node(mpz_get_str(NULL, 10, z));
}

void
lin_mpq_set_node(mpq_t q, node *n) {
  if (is_number(n)) {
    mpz_set_str(mpq_numref(q), get_value(n), 10);
    mpz_set_si(mpq_denref(q), 1);
  }
  else {
    mpz_set_str(mpq_numref(q), get_value(get_ith_child(n, 1)), 10);
    mpz_set_str(mpq_denref(q), get_value(get_ith_child(n, 2)), 10);
  }
}

void
lin_mpz_set_node(mpz_t z, node *n) {
  mpz_set_str(z, get_value(n), 10);
}

node *
lin_new_realnumber_node(double d) {
  char *str;
  lin_sprintf_smart(&str, "%g", d);
  return new_realnumber_node(str);
}

static void
lin_expr_calc_rec(mpq_t res, node *expr) {
  if (is_number(expr)) {
    mpq_set_str(res, get_value(expr), 10);
  }
  else if (is_realnumber(expr)) {
    double d = strtod(get_value(expr), NULL);
    mpq_set_d(res, d);
  }
  else if (is_list(expr)) {
    if (get_children_num(expr) == 3) {
      node *op = get_ith_child(expr, 0);
      mpq_t arg1, arg2;
      mpq_inits(arg1, arg2, NULL);
      lin_expr_calc_rec(arg1, get_ith_child(expr, 1));
      lin_expr_calc_rec(arg2, get_ith_child(expr, 2));
      if (lin_is_keyword_node(op, LIN_KEYWORD_PLUS)) {
        mpq_add(res, arg1, arg2);
      }
      else if (lin_is_keyword_node(op, LIN_KEYWORD_MINUS)) {
        mpq_sub(res, arg1, arg2);
      }
      else if (lin_is_keyword_node(op, LIN_KEYWORD_TIMES)) {
        mpq_mul(res, arg1, arg2);
      }
      else if (lin_is_keyword_node(op, LIN_KEYWORD_DIVIDE)) {
        mpq_div(res, arg1, arg2);
      }
      else if (lin_is_keyword_node(op, LIN_KEYWORD_MOD)) {
        mpz_t z1, z2, z;
        mpz_inits(z1, z2, z, NULL);
        mpq_get_den(z1, arg1);
        mpq_get_den(z2, arg2);
        if ((mpz_cmp_si(z1, 1) == 0) && (mpz_cmp_si(z2, 1) == 0)) {
          mpq_get_num(z1, arg1);
          mpq_get_num(z2, arg2);
          mpz_mod(z, z1, z2);
          mpq_set_num(res, z);
          mpz_set_si(z, 1);
          mpq_set_den(res, z);
        }
        else {
          lin_fatalerror(1, LIN_ERR_MSG_EXPR_CALC_MOD_RATIONAL);
        }
        mpz_clears(z1, z2, z, NULL);
      }
      mpq_clears(arg1, arg2, NULL);
    }
    else if (get_children_num(expr) == 2) {
      node *op = get_ith_child(expr, 0);
      if (lin_is_keyword_node(op, LIN_KEYWORD_MINUS)) {
        lin_expr_calc_rec(res, get_ith_child(expr, 1));
        mpq_neg(res, res);
      }
    }
  }
}

char *
lin_node_to_str(node *n) {
  char *res;
  if (is_list(n)) {
    size_t len = 0;
    char **res_list;
    LIN_MALLOC(res_list, get_children_num(n), char *);
    for (int i = 0; i < get_children_num(n); ++i) {
      res_list[i] = lin_node_to_str(get_ith_child(n, i)); 
      len += strlen(res_list[i]);
    }
    LIN_MALLOC(res, len + 1, char);
    len = 0;
    for (int i = 0; i < get_children_num(n); ++i) {
      strcpy(res + len, res_list[i]);
      len += strlen(res_list[i]);
      LIN_FREE(res_list[i]);
    }
    LIN_FREE(res_list);
  }
  else {
    res = strdup(get_value(n));
  }
  return res;
}
