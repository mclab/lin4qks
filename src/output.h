#ifndef LIN_OUTPUT_H_
#define LIN_OUTPUT_H_

extern void
lin_output_vars_list(FILE * /*stream*/, node * /*vars_list*/);

extern void
lin_output_formula(FILE * /*stream*/, node * /*formula*/, size_t /*shift_num*/);

#endif /* #ifndef LIN_OUTPUT_H_ */
